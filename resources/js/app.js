require("./bootstrap");

import Alpine from "alpinejs";
import Vue from "vue";
import Chessboard from "./components/Chessboard2.vue";
import ChessboardGame from "./components/ChessboardGame.vue";

window.Alpine = Alpine;

Alpine.start();

Vue.config.productionTip = false;
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

const app = new Vue({
    name: "app",
    components: {
        chessboardindex: Chessboard,
        chessboardgame: ChessboardGame,
    },
}).$mount("#app");
