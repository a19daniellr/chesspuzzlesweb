@extends('templates.master')

@section('title')
    Profile
@endsection

@section('headertitle')
    Profile stats, {{$data["user"]->name}}.
@endsection

@section('headerintro')
    Personal Information.
@endsection

@section('content')
    <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
        <div class="card-header">Stats</div>
        <div class="card-body">
            <h5 class="card-title">User´s Score</h5>
            @if ($data["stats"] != "")
                <p class="card-text">Score: {{$data["stats"]->score}}.</p>
            @else 
                <p class="card-text">No Score go play some puzzles !</p>
            @endif
            
        </div>
    </div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Puzzle</th>
            <th scope="col">Solved</th>
        </tr>
        </thead>
        <tbody>
            @if (count($data["solveds"]) > 0)
                @for ($i = 0; $i < count($data["solveds"]); $i++)
                    <tr>
                        <th scope="row">{{$data["solveds"][$i]->puzzle_id}}</th>
                        <td>{{$data["solveds"][$i]->solved == 1 ? "SOLVED :)" : "FAILED :("}}</td>
                    </tr>
                @endfor
            @else
                <tr>
                    <th>NO PUZZLES SOLVED</th>
                    <td>GO PLAY SOME PUZZLES !</td>
                </tr>
            @endif
        
        </tbody>
    </table>
@endsection
