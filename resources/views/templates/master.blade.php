<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>@yield('title')</title>
    {{-- <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" /> --}}
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />

</head>

<body>
    <!-- Responsive navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container px-5">
            <a class="navbar-brand" href="{{ route('puzzles.index') }}">Chess Puzzles</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item"><a class="nav-link" aria-current="page"
                            href="{{ route('puzzles.index') }}">Home</a>
                    </li>
                    @auth
                        <li class="nav-item"><a class="nav-link" aria-current="page"
                            href="{{ route('users.index') }}">Profile</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" aria-current="page" href="#"
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                        </li>
                    @endauth
                    @yield('navbarlinks')
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content-->
    <div class="container px-4 px-lg-5">
        <!-- Heading Row-->
        <div class="row gx-4 gx-lg-5 align-items-center my-5">
            <div class="col-lg-7"><img class="img-fluid rounded mb-4 mb-lg-0"
                    src="{{ asset('storage/images/wolf.jpg') }}"
                    alt="Chess image to land page, cat with chessboard." /></div>
            <div class="col-lg-5">
                <h1 class="font-weight-light">@yield("headertitle")</h1>
                <p>@yield('headerintro')</p>
            </div>
        </div>
        <!-- Content Row-->
        <div class="row gx-4 gx-lg-5">
            @yield('content')
        </div>
    </div>
    <!-- Footer-->
    <footer class="py-5 bg-dark">
        <div class="container px-4 px-lg-5">
            <p class="m-0 text-center text-white">Copyright &copy; Chess Puzzles 2022 DANIEL LAGO RIOMAO</p>
        </div>
    </footer>
    <form method="POST" id="logout-form" action="{{ route('logout') }}">
        @csrf
    </form>
    <script src="{{ asset('js/app.js') }}"></script>

</body>

</html>
