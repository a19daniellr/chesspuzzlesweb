@extends('templates.master')

@section('title')
    ChessPuzzles
@endsection

@section('navbarlinks')
    @guest
        <li class="nav-item"><a class="nav-link" aria-current="page" href="{{ route('login') }}">Login</a></li>
        <li class="nav-item"><a class="nav-link" aria-current="page" href="{{ route('register') }}">Sign in</a></li>
    @endguest
@endsection

@section('headertitle')
    Good luck with this puzzle !
@endsection

@section('headerintro')
    Rating: {{ $data['puzzle']->rating }}
    <hr />
    Popularity: {{ $data['puzzle']->popularity }}
    <hr />
    Themes:
    <ul>
        @foreach ($data['themes'] as $theme)
            <li>{{ $theme }}</li>
        @endforeach
    </ul>
    <hr />
    @php
    $color = explode(' ', $data['puzzle']->fen)[1];
    if ($color === 'w') {
        $color = 'BLACK';
    } else {
        $color = 'WHITE';
    }
    @endphp
    <h1 class="display-6" style="text-align: center">You move with <strong>{{ $color }}</strong></h1>
@endsection

@section('content')
    <div id="app">
        <chessboardgame :idUser="'{{Auth::id()}}'" :idPuzzle="'{{$data['puzzle']->id}}'" :moves="'{{ $data['puzzle']->moves }}'" :fen="'{{ $data['puzzle']->fen }}'" />
    </div>
@endsection
