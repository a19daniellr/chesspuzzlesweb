@extends('templates.master')

@section('title')
    ChessPuzzles
@endsection

@section('navbarlinks')
    @guest
        <li class="nav-item"><a class="nav-link" aria-current="page" href="{{ route('login') }}">Login</a></li>
        <li class="nav-item"><a class="nav-link" aria-current="page" href="{{ route('register') }}">Sign in</a></li>
    @endguest
@endsection

@section('headertitle')
    Welcome to CHESS PUZZLES.
@endsection

@section('headerintro')
    This is the landpage of my project. I hope you enjoy it playing all the puzzles you can.
    REMEMBER ! You can save your points and stats if you signin !
@endsection

@section('content')
    <div class="form-group">
        <label for="search">Search</label>
        <input required placeholder="Insert min value" class="form-control" type="number" name="search" id="search">
    </div>
    <div class="form-group">
        <label for="search-select">Category</label>
        <select class="form-select" name="search-select" id="search-select">
            <option value="popularity">Popularity</option>
            <option value="rating">Rating</option>
        </select>
    </div>
    <div class="form-group">
        <label for="order-select">Order by</label>
        <select class="form-select mb-3" name="order-select" id="order-select">
            <option value="asc">Ascending</option>
            <option value="desc">Descending</option>
        </select>
        <button class="btn btn-outline-primary float-end mb-4" onclick="filter()">Search</button>
    </div>
    <div id="app">
        <chessboardindex :puzzles="{{ $puzzles }}" />
    </div>
    <script>
        function filter() {
            const search = document.getElementById("search").value;
            const select = document.getElementById("search-select").value;
            const order = document.getElementById("order-select").value;
            window.location.href = "/puzzles/" + select + "/" + search + "/" + order;
        }
    </script>
@endsection
