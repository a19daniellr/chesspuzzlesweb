<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Solved extends Pivot
{
    use HasFactory;

    protected $fillable = array("puzzle_id", "user_id", "solved");
}
