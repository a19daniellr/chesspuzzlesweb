<?php

namespace App\Http\Controllers;

use App\Models\Solved;
use App\Models\Stat;
use Illuminate\Http\Request;

class StatsController extends Controller
{
    public function store(Request $request) {
        $exist = Stat::where("user_id", "=", $request->user_id)->get();
        $solved = Solved::where("user_id", "=", $request->user_id)->where("puzzle_id", "=", $request->puzzle_id)->get();
        if (count($solved) == 0 && $request->solved) {
            if (count($exist) == 0) {
                $stats = new Stat;
                $stats->score = 500 + intval($request->score);
                $stats->user_id = $request->user_id;
                $stats->save();
            } else {
                $exist[0]->score += intval($request->score);
                $exist[0]->update();
            }
        }
    }
}
