<?php

namespace App\Http\Controllers;

use App\Models\Solved;
use DateTime;
use Exception;
use Illuminate\Http\Request;

class SolvedController extends Controller
{
    public function store(Request $request) {
        $exist = Solved::where("user_id", "=", $request->user_id)->where("puzzle_id", "=", $request->puzzle_id)->get();
        if (count($exist) == 0) {
            $solved = new Solved;
            $solved->puzzle_id = $request->puzzle_id;
            $solved->user_id = $request->user_id;
            $solved->solved = $request->solved;
            $solved->resolution_date = date("Y-m-d H:i:s");
            $solved->save();
        }
    }
}
