<?php

namespace App\Http\Controllers;

use App\Models\Puzzle;
use App\Models\PuzzleTheme;
use App\Models\Theme;
use Illuminate\Http\Request;

class PuzzleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $max = Puzzle::all()->count();
        $skip = random_int(0, $max - 20);
        $puzzles = Puzzle::skip($skip)->take(20)->get();

        return view("puzzles.index")->with("puzzles", $puzzles);
    }

    public function getWithFilter($category, $name, $order) {
        if ($name && $category && $order && is_numeric($name)) {
            $puzzles = Puzzle::where($category, ">", $name)->orderBy($category, $order)->get();
            return view("puzzles.index")->with("puzzles", $puzzles);
        } else {
            $max = Puzzle::all()->count();
            $skip = random_int(0, $max - 20);
            $puzzles = Puzzle::skip($skip)->take(20)->get();

            return view("puzzles.index")->with("puzzles", $puzzles);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $puzzle = Puzzle::where("id", "=", $id)->firstOrFail();

        $themesIds = PuzzleTheme::where("puzzle_id", "=", $id)->get();
        $themesIdsLength = PuzzleTheme::where("puzzle_id", "=", $id)->count();

        $themes = array();

        for ($i = 0; $i < $themesIdsLength; $i++) {
            $theme = Theme::where("id", "=", $themesIds[$i]->theme_id)->firstOrFail();
            $themes[] = $theme->name;
        }

        $data = [
            "puzzle" => $puzzle,
            "themes" => $themes,
        ];

        return view("puzzles.show")->with("data", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
