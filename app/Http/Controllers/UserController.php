<?php

namespace App\Http\Controllers;

use App\Models\Solved;
use App\Models\Stat;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index() {
        $user = auth()->user();
        $userPuzzle = User::where("email", "=", $user->email)->get();
        $stats = Stat::where("user_id", "=", $userPuzzle[0]->id)->get();
        $solveds = Solved::where("user_id", "=", $userPuzzle[0]->id)->get();
        $data = [
            "user" => $userPuzzle[0],
            "stats" => count($stats) > 0 ? $stats[0] : "",
            "solveds" => $solveds,
        ];
        return view("users.index")->with("data", $data);
    }
}
