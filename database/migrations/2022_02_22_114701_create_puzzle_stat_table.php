<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePuzzleStatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solved', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("puzzle_id");
            $table->unsignedBigInteger("user_id");
            $table->boolean("solved");
            $table->timestamp("resolution_date")->useCurrent();
            $table->timestamps();

            $table->foreign("puzzle_id")->references("id")->on("puzzles")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade")->onUpdate("cascade");        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solved');
    }
}
