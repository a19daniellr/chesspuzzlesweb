<?php

namespace Database\Factories;

use App\Models\Puzzle;
use App\Models\Solved;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SolvedFactory extends Factory
{
    protected $model = Solved::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $puzzles = Puzzle::all()->count();
        $users = User::all()->count();
        return [
            "puzzle_id" => $this->faker->unique()->numberBetween(1, $puzzles),
            "user_id" => $this->faker->unique()->numberBetween(1, $users),
            "solved" => $this->faker->boolean(),
        ];
    }
}
