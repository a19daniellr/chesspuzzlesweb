<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PuzzleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    // protected $fillable = array("fen", "moves", "rating", "web", "popularity");
    public function definition()
    {
        return [
            "fen" => "r1bqk2r/pp1nbNp1/2p1p2p/8/2BP4/1PN3P1/P3QP1P/3R1RK1 b kq - 0 19",
            "moves" => "e8f7 e2e6 f7f8 e6f7",
            "rating" => "1588",
            "web" => "https://lichess.org/71ygsFeE/black#38",
            "popularity" => "88"
        ];
    }
}
