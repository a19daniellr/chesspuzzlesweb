<?php

use App\Http\Controllers\PuzzleController;
use App\Http\Controllers\SolvedController;
use App\Http\Controllers\StatsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/puzzles');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return redirect('/puzzles');
});

Route::resource("puzzles", PuzzleController::class);

Route::get('/puzzles/{category}/{name}/{order}', [PuzzleController::class, 'getWithFilter']);

Route::resource("solveds", SolvedController::class);

Route::resource("stats", StatsController::class);

Route::resource("users", UserController::class);
